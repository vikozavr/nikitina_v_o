#include <iostream>
#include <vector>
#include <algorithm>
#include "QueueR.h"

QueueR::QueueR(std::vector <int> qu){

    std::sort(qu.begin(), qu.end());
    pupa = new Node(qu[0], nullptr);
    Node* next_n = pupa;
    len = qu.size();
    for(int i = 1; i <= qu.size(); i++){
       Node* pupa_a = new Node(qu[i], nullptr);
       next_n->next = pupa_a;
    }


}
QueueR::QueueR(){
    pupa = nullptr;
    len = 0;
}



QueueR::Node::Node(int a, Node* next){
    this->a = a;
    this->next = next;
}
 QueueR::Node::~Node(){
    delete next;

}
bool QueueR::isEmpty() const{
    if (len==0) {return true;}
    else{ return false;}
}

void QueueR::push(int a){
    if (isEmpty()){
        pupa = new Node(a,nullptr);
        len+=1;
    }
    else{Node* next1 = pupa;
        while((a > next1->a) && (next1->next != nullptr)){
            next1 = next1->next;
        }
        next1->next = new Node(a, next1->next);
    }

}

int QueueR::pop(){
    if (isEmpty()){
        return 0;
    }
    else{
        int b = pupa->a;
        Node* n = pupa->next;
        delete pupa;
        --len;
        pupa = new Node(b,n);
        return b;
    }
}
int QueueR::top() const{
    if (isEmpty()){
        return 0;
    }
    else{
        return pupa->a;
    }
}

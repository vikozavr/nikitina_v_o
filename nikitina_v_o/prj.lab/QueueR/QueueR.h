#ifndef OOP3_QUEQUER_H
#define OOP3_QUEQUER_H


#include <iostream>
#include <vector>
class QueueR{
private:
    class Node{
    public:
        Node(int a, Node* next);
        ~Node();

    private:
        int a = 0;
        Node* next = nullptr;
        friend class QueueR;
    };
    Node* pupa;
    int len;


public:

    QueueR(std::vector <int> qu);//+
    QueueR(); //+
//~QueueR();
    void push(int a);  //+
    int pop();//+
    bool isEmpty() const; //+
    int top() const;//+

};



#endif //OOP3_QUEQUER_H
